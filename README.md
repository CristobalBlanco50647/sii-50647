# README #

This README would normally document whatever steps are necessary to get your application up and running.

### SITUACIÓN ACTUAL DE JUEGO TENIS: ###

* JUGAR AL JUEGO TENIS.
* Version 5.1: SISTEMA DISTRIBUIDO DEL JUEGO TENIS PARA VARIOS CLIENTES ESTABLECIDO (ARREGLADO EL BUG DE VELOCIDADES)

### CAMBIOS REALIZADOS ###

* v1.1: añade movimiento a esferas y raquetas (jugador izquierda se mueve arriba abajo con w/s respectivamente y el derecha con o/l)
* v1.2: añade reduccion de radio a las esferas a medida que pasa el tiempo.
* v1.3 / v1.4: introduccion de aumento del numero de esferas a lo largo del tiempo (máximo 16).
* v1.5: añade disparos a los jugadores (maximo de 1 proyectil en el campo). Jugador izquierda pulsa q y derecha pulsa. Añade una lista changelog.txt.
* v2.1: añade el logger.cpp, que contiene un FIFO que se comunica con mundo.cpp. Logger.cpp leera la informacion de la tuberia e imprimira quien marco y su puntuación. Establece también la puntuación maxima para finalizacion del juego (10 puntos).
* v3.0: Introducción de FIFOS para la comunicación entre servidor y cliente.
* v4.0: Intoducción de sockets servidor y cliente (eliminación de las fifos)
* v5.0: Intoducción de aceptacion de varios clientes para el servidor (dos personas jugaran, el resto seran espectadores). BUG CON EL PROCESO DE VELOCIDADES.

* v5.1 (VERSION FINAL HECHA EN EL MERGE DE LA PRACTICA 5): Arreglo del bug de velocidades.

### Profesores de SII. ###

* Raquel Cedazo
* Angel Rodriguez