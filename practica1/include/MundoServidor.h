// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_SERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "Socket.h"
#include <fcntl.h>
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DatosMemCompartida.h"
//#include "Esfera.h"
//#include "Raqueta.h"
// Includes para los sockets //
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include "Socket.h"
class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
        void RecibeComandosJugador1();
	void RecibeComandosJugador2();
        char mensaje[200];
        char cad[200];
        char nombre[200];
        int fd;
        int fd_memoria;
        int fd_dibujo; // FIFO PARA DAR COORDENADAS //
        int fd_teclas; // FIFO PARA DAR TECLAS AL THREAD //
       // int sd; // SOCKET DE CONEXION
       // int sc; // SOCKET DE COMUNICACION CON EL CLIENTE.
       // struct sockaddr_in server_addr,client_addr;
       // DECLARACION DE LOS SOCKETS SERVIDOR Y CLIENTE.
        Socket servidor;
        std::vector<Socket> conexiones;
	void GestionaConexiones();
	
	std::vector <Esfera> ListaEsferas;
	std::vector<Plano> paredes;
        std::vector <Esfera> ListaDisparo1;
        std::vector <Esfera> ListaDisparo2;
        float timer1;
        float timer2;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
        pthread_t thid1; // jugador 1
	pthread_t thid2; // jugador 2
	pthread_t thid_conexiones;

	int puntos1;
	int puntos2;
	int acabar;

        DatosMemCompartida Datos;
        DatosMemCompartida *PunteroDatos;
        struct stat bstat;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
