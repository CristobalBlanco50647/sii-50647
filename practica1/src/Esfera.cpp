// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{
radio=0.0f;
velocidad.x=0;
velocidad.y=0;
}



void Esfera::Dibuja()
{
if((velocidad.x==2)&&(velocidad.y==0))
glColor3ub(0,255,0);
else if((velocidad.x==-2)&&(velocidad.y==0))
glColor3ub(255,0,0);
else
glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro.x+=t*velocidad.x;
centro.y+=t*velocidad.y;
if(radio>=0.1)
radio-=0.0005;
}

