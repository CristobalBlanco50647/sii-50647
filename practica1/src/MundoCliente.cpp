// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoCliente.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
if (munmap(PunteroDatos,bstat.st_size) == -1)
   	{
   	  close(fd_memoria);
  	  perror("Error un-mmapping the file");
   	}
        close(fd_memoria);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
        char tipo[20];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
        sprintf(tipo,"CLIENTE");
        print(tipo,375,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
/// LISTA DE ESFERAS ///////
	for(i=0;i<ListaEsferas.size();i++)
                ListaEsferas[i].Dibuja();
for(i=0;i<ListaDisparo1.size();i++)
                ListaDisparo1[i].Dibuja();
for(i=0;i<ListaDisparo2.size();i++)
                ListaDisparo2[i].Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}
float contador=0.0f;





void CMundo::OnTimer(int value)
{	

servidor.Receive(cad,sizeof(cad));
sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f", &ListaEsferas[0].centro.x,&ListaEsferas[0].centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2, &ListaEsferas[0].radio);
if((puntos1>=10)||(puntos2>=10))
{
exit(0);
}
////////////////////// DECISIONES DEL BOT A REALIZAR PROYECCION DEL FICHERO ////////////////////////
Datos.esfera=ListaEsferas[0];
Datos.raqueta1=jugador2;
PunteroDatos->esfera=ListaEsferas[0];
PunteroDatos->raqueta1=jugador2;	
if(PunteroDatos->accion==1) OnKeyboardDown('o',1,1);
if(PunteroDatos->accion==-1) OnKeyboardDown('l',1,1);
}





void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
        sprintf(cadteclas,"%c",key);
        servidor.Send(cadteclas,sizeof(cadteclas));
}

void CMundo::Init()
{
printf("INTRODUZCA NOMBRE DEL CLIENTE:\n");
gets(nombre);
servidor.Connect((char *)"127.0.0.1",4800);
servidor.Send(nombre,sizeof(nombre));


if(ListaEsferas.empty())
ListaEsferas.push_back(Esfera());
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
    
/////////////////////// PROYECCION EN FICHEROS PARA EL BOT //////////////////////////   
Datos.esfera=ListaEsferas[0];
Datos.raqueta1=jugador2;		
void *org;
if((fd_memoria=open("/tmp/bot.txt", O_CREAT|O_RDWR,0666))<0)
perror("Error al abrir/crear fichero");
write(fd_memoria,&Datos, sizeof(Datos));	
if (fstat(fd_memoria, &bstat)<0) {
perror("Error en fstat del fichero"); close(fd_memoria);
}
printf("tamanio fichero %ld ",bstat.st_size);

//Proyeccion del fichero

if((org=mmap((caddr_t) 0,bstat.st_size, PROT_READ |PROT_WRITE ,MAP_SHARED,fd_memoria,0))==MAP_FAILED){ 
perror("Error en la proyeccion del fichero"); close(fd_memoria);
}
else printf("mem proyectada\n");
close(fd_memoria);
PunteroDatos=static_cast<DatosMemCompartida*>(org);
}

