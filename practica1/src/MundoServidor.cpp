// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoServidor.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}

void* hilo_conexiones(void* dconexiones)
{
      CMundo* pconexiones=(CMundo*) dconexiones;
      pconexiones->GestionaConexiones();
}

CMundo::CMundo()
{
	Init();
        fd=open("/tmp/mififo1",O_WRONLY);
}

CMundo::~CMundo()
{
close(fd);
acabar=1;
pthread_join(thid1,NULL);
pthread_join(thid2,NULL);
pthread_join(thid_conexiones,NULL);
for(int i=0;i<conexiones.size();i++){
	conexiones[i].Close();
}
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	char tipo[20];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
        sprintf(tipo,"SERVIDOR");
        print(tipo,375,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
/// LISTA DE ESFERAS ///////
	for(i=0;i<ListaEsferas.size();i++)
                ListaEsferas[i].Dibuja();
for(i=0;i<ListaDisparo1.size();i++)
                ListaDisparo1[i].Dibuja();
for(i=0;i<ListaDisparo2.size();i++)
                ListaDisparo2[i].Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

//////////////////////////////////////////////// ONTIMER FUNCTION /////////////////////////////////////
void CMundo::OnTimer(int value)
{	
int i;
int j;
jugador1.Mueve(0.025f);
jugador2.Mueve(0.025f);
for(i=0;i<ListaEsferas.size();i++)
{
        ListaEsferas[i].Mueve(0.025f);
        jugador1.Rebota(ListaEsferas[i]);
	jugador2.Rebota(ListaEsferas[i]);
	if(fondo_izq.Rebota(ListaEsferas[i]))
		{
			ListaEsferas[i].centro.x=0;
			ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			ListaEsferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
                	puntos2++;
			sprintf(mensaje,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
                	write(fd,mensaje,sizeof(mensaje));
		}
	if(fondo_dcho.Rebota(ListaEsferas[i]))
		{
			ListaEsferas[i].centro.x=0;
			ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			ListaEsferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
                	sprintf(mensaje,"El jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
                	write(fd,mensaje,sizeof(mensaje));
        	}
}
//esfera.Mueve(0.025f);
for(i=0;i<paredes.size();i++)
{
        for(j=0;j<ListaEsferas.size();j++)
                paredes[i].Rebota(ListaEsferas[j]);
		//paredes[i].Rebota(esfera);
	paredes[i].Rebota(jugador1);
	paredes[i].Rebota(jugador2);
}
        for(i=0;i<ListaEsferas.size();i++)
                ListaEsferas[i].Mueve(0.025f);
if((puntos1>=10)||(puntos2>=10)) exit(0);


sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f", ListaEsferas[0].centro.x,ListaEsferas[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2, ListaEsferas[0].radio);
////////////////////////// ESTABLECIENDO CONTROL DE LAS CONEXIONES ////////////////////////////

for (int w=conexiones.size()-1; w>=0; w--) 
{
        if (conexiones[w].Send(cad,200) <= 0) {
	    printf("CLIENTE DESCONECTADO DEL SERVIDOR\n");
            conexiones.erase(conexiones.begin()+w);
            if (w < 2) // Hay menos de dos clientes conectados
	    {
		printf("REAJUSTANDO EL MARCADOR\n");
		puntos2=0;
		puntos1=0;
	    }
         }
}
}


void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
}

void CMundo::Init()
{
int size;

acabar=0;
servidor.InitServer((char *)"127.0.0.1",4800);

if(ListaEsferas.empty())
ListaEsferas.push_back(Esfera());
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
        
        pthread_create(&thid1, NULL, hilo_comandos1, this);
	pthread_create(&thid2, NULL, hilo_comandos2, this);
	pthread_create(&thid_conexiones, NULL, hilo_conexiones, this);
}


void CMundo::RecibeComandosJugador1()
{
char jug1[200];
     while (!acabar)
	{
            usleep(25000);
            if(conexiones.size()>0)
		{
            		conexiones[0].Receive(jug1, 200);
            		unsigned char comando1;
            		sscanf(jug1,"%c",&comando1);
            		if(comando1=='s')jugador1.velocidad.y=-4;
            		if(comando1=='w')jugador1.velocidad.y=4;
		}
      }
}

void CMundo::RecibeComandosJugador2()
{
char jug2[200];
     while (!acabar) 
	{
            usleep(25000);
	    if(conexiones.size()>1)
		{
            		conexiones[1].Receive(jug2, 200);
            		unsigned char comando2;
            		sscanf(jug2,"%c",&comando2);
            		if(comando2=='l')jugador2.velocidad.y=-4;
            		if(comando2=='o')jugador2.velocidad.y=4;
		}
      }
}

void CMundo::GestionaConexiones()
{
	while(!acabar)
	{
	usleep(25000);
	conexiones.push_back(servidor.Accept());
	conexiones[conexiones.size()-1].Receive(nombre,sizeof(nombre));
	printf("CLIENTE %s SE HA CONECTADO AL SERVIDOR\n",nombre);
	}
}
